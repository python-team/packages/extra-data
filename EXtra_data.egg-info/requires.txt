h5py>=2.10
matplotlib
numpy
packaging
pandas
xarray
pyyaml

[bridge]
karabo-bridge>=0.6
psutil

[complete]
dask[array]
extra_data[bridge]

[complete:python_version < "3.11"]
tomli

[docs]
extra_data[bridge]
ipython
nbsphinx
sphinx
sphinxcontrib_github_alt

[test]
cloudpickle
coverage
extra_data[complete]
nbval
pytest
pytest-cov
testpath
